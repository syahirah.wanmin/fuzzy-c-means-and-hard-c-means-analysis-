# Fuzzy C Means and Hard C Means Analysis 



There are so many problems of real life that need us to find the solution. For example, is clustering by using FCM and HCM. FCM and HCM is a method for clustering the dataset. The raw or unlabeled data from the large of dataset can be classified in and unsupervised by using cluster analytics. The main objective of the clustering is to group the data in multivariate of dataset such as the similarities are maximized within same cluster and minimized between different clusters. Data clustering analysis can be used as a stand-alone data mining tool, or as a pre-processing step for other data mining algorithms. However, these algorithms have their own disadvantages as reported by recent studies. The performance of clustering is determined not only by the geometrical shapes and densities of the individual clusters but also by spatial relations and distances among the clusters. As for our project, we have decided on trying to classify the El Nino strength by using FCM and HCM method. El Nino is a climate cycle in the Pacific Ocean with global impact on weather pattern. It is an irregularly occurring and complex series of climatic changes affecting the equatorial Pacific region and beyond every few years, characterized by the appearance of unusually warm, nutrient-poor water off north of Peru and Ecuador, typically in late of December, in this project the attribute is specified on the sea surface.

Dataset that we used is the **El Nino Dataset from kaggle.com** which consists of **178080 datasets with 12 attributes**. The objective of the project is to cluster the El Nino dataset which each clustering group represents the strength of the phenomenon.The clustering results then will be analyzed to study the impacts of each strength level of El Nino phenomenon towards the environments. The problem statement we discovered is what is the pattern of clustering group of El Nino dataset based on the given attributes. Secondly, what is the impact of each of El Nino strength towards the environments based on the real past case researches.

In order to solve the problem, we proposed the **clustering method using Fuzzy C-Means (FCM) and Hard C-Means (HCM)**. By using these two methods, we discovered which method gives the best accuracy on clustering the El Nino dataset which each group represents the El Nino strength. Finally, we conclude that clustering using FCM method is better than HCM method based on the results we analyzed.


Objectives
The solution proposed in this paper consists of four major objectives:
1. To cluster the El Nino dataset using Fuzzy C Mean (FCM) and Hard C Mean (HCM) which each clustering group represents the strength of El Nino.
2. To analyse the effect of the El Nino clustering group on the real data of El Nino phenomenon.
3. To identify which method is more accurate between FCM and HCM method on clustering the dataset.

At the end of the proect, we compared the HCM and FCM based on their statistical (**Accuracy,RMSE and RMSD**) and visualisation (**Clustering graph of 3CG**).

With the results observation, the clustering results are being compared and analysed with the real data we obtained from **Oceanic Nino Index (ONI)** and **Climate.org.**

Disclaimer: 

Credit to my AWWWsome team members - Allik, Nadiah, Akmal! 